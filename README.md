## Greg's Grooves Demo
This is a demo app called **Greg's Grooves**. This app is a working example of a reactive and responsive app that consumes and manipulates json data.

Greg has a lot of records, he needs some easy way to see them all, update and add new ones. He clearly enjoys very unique vinyl so his management software should reflect that, while being intuitive and easy to use.

## Demo ##

[Launch demo site](http://gregs-grooves.drewriley.com/)

## Screenshots
![Normal mode](https://vaccines.werdpress.ca/img/icons/screengrab1.jpg)
![Mini mode](https://vaccines.werdpress.ca/img/icons/screengrab2.jpg)
![logo](https://vaccines.werdpress.ca/img/icons/gregs-grooves.png)


## Tech/framework used

**Built with**
> [vue js](https://cli.vuejs.org/)
-
> Vue Router
-
> VueX
-
> [Vuetify](https://vuetifyjs.com/)
-



## Installation

Ensure your [vue cli](https://cli.vuejs.org/guide/installation.html) is up to date, then download or clone this repository to your system.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```


## License
This is a demo app, feel free to use it for eduction or demonstration purposes. You may not use it for any comercial product.

MIT © [Drew Riley]()